﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task2Algorithms;

namespace BinaryMethodTest
{
    [TestClass]
    public class UnitTestBinaryMethod
    {
        /// <summary>
        /// check for correct operation of the function WritebackBinaryNumber
        /// </summary>
        [TestMethod]
        public void WritebackBinaryNumberTest()
        {
            int number = 11;
            int expected = 1011;
            int actual = BinaryMethod.WritebackBinaryNumber(number);
            Assert.AreEqual(expected, actual);
        }
      
        /// <summary>
        /// checks when the function is set to true
        /// </summary>
        [TestMethod]
        public void CheckInputStringOnRegexTestIsTrue()
        {
            string str = "25";
            bool actual = BinaryMethod.CheckInputStringOnRegex(str);
            Assert.IsTrue(actual);
        }

        /// <summary>
        /// checks when the function is set to false
        /// </summary>
        [TestMethod]
        public void CheckInputStringOnRegexTestIsFalse()
        {
            string str = "2.5";
            bool actual = BinaryMethod.CheckInputStringOnRegex(str);
            Assert.IsFalse(actual);
        }
    }
}
