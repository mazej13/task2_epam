﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Task2Algorithms
{
    /// <summary>
    /// Algorithm Newton's method
    /// </summary>
    public class NewtonMethod
    {

        /// <summary>
        /// check for negative numbers and odd parity power
        /// </summary>
        /// <returns>returns 1 when number and power less 0, 2- number less 0,3- power less 0 </returns>
        private static void СheckNegativeNumbersAndParityPower(ref double number, ref int power, out int answer)
        {
            double modulo = power % 2;
            answer = 0;
            if (number < 0 && modulo == 0) throw new Exception();

            if (number < 0 && power < 0)
            {
                number = -number;
                power = -power;
                answer = 1;
            }

            if (number < 0)
            {
                number = -number;
                answer = 2;
            }

            if (power < 0)
            {
                power = -power;
                answer = 3;
            }
        }

        /// <summary>
        /// Sets the function for Newton's method
        /// </summary>
        /// <param name="argument">argument of the function for Newton's method</param>
        /// <param name="number">the number of which is taken sqr</param>
        /// <param name="exponent">power of the sqr</param>
        /// <returns>dunction for Newton's method</returns>
        private static double Function(double argument, double number, int exponent)
        {
            return Math.Pow(argument, exponent) - number;
        }

        /// <summary>
        /// derivative of the function for Newton's method
        /// </summary>
        /// <param name="argument">argument of the function for Newton's method</param>
        /// <param name="exponent">power of the sqr</param>
        /// <returns>derivative of the function</returns>
        private static double DecFunction(double argument, int exponent)
        {
            return exponent * Math.Pow(argument, exponent - 1);
        }

        /// <summary>
        /// Algorithm for finding the sqr of the n-th power Newton's method
        /// </summary>
        /// <param name="number"></param>
        /// <param name="exponent"></param>
        /// <returns> value sqr</returns>
        public static double MethodNewtons(double number, int exponent)
        {    
            if (number == 0) return 0;
            if (number == 1 || exponent == 0) return 1;
           
            int answer;
            СheckNegativeNumbersAndParityPower(ref number, ref exponent, out answer);
           
            try
            {
                double sqrt = CalculateSqrt(number, exponent);

                switch (answer)
                {
                    case 1: sqrt = 1 / (-sqrt); break;
                    case 2: sqrt = -sqrt; break;
                    case 3: sqrt = 1 / sqrt; break;
                }
                return sqrt;               
            }
            catch (Exception)
            {
                throw new Exception("Incorrect input values");
            }
        }

        /// <summary>
        /// Calculate sqrt N power
        /// </summary>
        /// <param name="number"></param>
        /// <param name="exponent"></param>
        /// <returns>value sqrt</returns>
        private static double CalculateSqrt(double number, int exponent)
        {
            double localValue = 0;
            double argument = 1;

            const double accuracy = 1E-15;
            do
            {
                localValue = argument;
                argument = argument - Function(argument, number, exponent) / DecFunction(argument, exponent);
            } while (Math.Abs(localValue - argument) > accuracy);

            return argument;
        }
    }
}
