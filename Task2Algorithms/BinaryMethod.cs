﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Task2Algorithms
{
    /// <summary>
    /// Translation decimal number to binary
    /// </summary>
   public class BinaryMethod
    {


       /// <summary>
       /// writeback binary number
       /// </summary>
        /// <param name="number">decimal number</param>
       /// <returns>binary number</returns>
        public static int WritebackBinaryNumber(int number)
        {
             
            int modulo = 0;
            List<int> moduloNumberIntroTwo = new List<int>();            
            string result = string.Empty;

            while (number > 0)
            {
                modulo = number % 2;
                number /= 2;
                moduloNumberIntroTwo.Add(modulo);

            }

            return NormalRecordingBinaryNumber(moduloNumberIntroTwo);
        }

       /// <summary>
        /// normal recording binary number
       /// </summary>
        /// <param name="moduloNumber">remainder of the division into two</param>
       /// <returns>binary number</returns>
        private static int NormalRecordingBinaryNumber(List<int> moduloNumber)
        { 
            int[] charNumbersString = new int [moduloNumber.Count];

            for (int i = moduloNumber.Count-1; i >=0; i--)
            {
                charNumbersString[moduloNumber.Count-1-i] = moduloNumber[i];
            }

            return Convert.ToInt32(string.Join<int>("",charNumbersString));
        }
    }
}
