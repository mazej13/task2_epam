﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Task2Algorithms;

namespace MethodNewton
{
    class Program
    {
     
        static void Main(string[] args)
        {
            string str = null;

            try
            {
            
                Console.Write("input number \t");
                str = Console.ReadLine();
                double number;
                int exponent;
                
                number = Convert.ToDouble(str);

                Console.Write("input exponent \t");
                str = Console.ReadLine();
                exponent = Convert.ToInt32(str);

                double newtonResult = NewtonMethod.MethodNewtons(number, exponent);
                double powResult = Math.Pow(number, (double)1 / exponent);

                Console.WriteLine("Result Newton: " + newtonResult+"\t");
                Console.WriteLine("Result Math.Pow: " + powResult+"\t");
            }
            catch 
            {
                Console.WriteLine("incorrect input data"); 
            }

            Console.ReadKey();
        }
    }
}
