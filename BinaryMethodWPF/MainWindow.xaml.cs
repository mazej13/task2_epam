﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using Task2Algorithms;

namespace BinaryMethodWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
                
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            int number = 0;
            string str = textBox1.Text;

            try
            {
                number = Convert.ToInt32(str);
                textBox2.Text = Convert.ToString(BinaryMethod.WritebackBinaryNumber(number));
            }
            catch (Exception)
            {
                MessageBox.Show("Incorrect intput number", "Error application", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
