﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task2Algorithms;

namespace MethodNewtonTest
{
    [TestClass]
    public class NewtonTest
    {
        /// <summary>
        /// check the correctness of the function Method Newton
        /// </summary>
        [TestMethod]
        public void MethodNewtonsTest()
        {
            double number = 100;
            int exponent = 2;
            double expented = 10;
            Assert.AreEqual(NewtonMethod.MethodNewtons(number, exponent), expented);
        }

        [TestMethod]
        public void MethodNewtonsTest1()
        {
            double number = 1000;
            int exponent = 3;
            double expented = 10;
            Assert.AreEqual(NewtonMethod.MethodNewtons(number, exponent), expented);
        }
        /// <summary>
        /// checks when the function is set to false
        /// </summary>
        [TestMethod]
        public void RegexTestIsFalse()
        {
            string str = "hello world";
            bool actual = NewtonMethod.CheckInputStringOnRegex(str);
            Assert.IsFalse(actual);
        }

        /// <summary>
        /// checks when the function is set to true
        /// </summary>
        [TestMethod]
        public void RegexTestIsTrue()
        {
            string str = "-12,455";
            bool actual = NewtonMethod.CheckInputStringOnRegex(str);
            Assert.IsTrue(actual);
        }
    }
}
